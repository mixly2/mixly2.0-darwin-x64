DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "当前脚本路径: $DIR"

GIT_DIR="$DIR/.git"
GIT_MIXLY_DIR="$DIR/.git_mixly"
GIT_DARWIN_AVR_DIR="$DIR/.git_darwin_avr"
GIT_DARWIN_ESP8266_DIR="$DIR/.git_darwin_esp8266"
GIT_DARWIN_ESP32_DIR="$DIR/.git_darwin_esp32"
GIT_ARDUINO_LIBS_DIR="$DIR/.git_arduino_libs"
GIT_CLONE_DIR="$DIR/gitDir"

if [ -d "$GIT_DIR" ]; then
	if [ ! -d "$GIT_MIXLY_DIR" ]; then
		mv ".git" ".git_mixly"
	elif [ ! -d "$GIT_DARWIN_AVR_DIR" ]; then
		mv ".git" ".git_darwin_avr"
	elif [ ! -d "$GIT_DARWIN_ESP8266_DIR" ]; then
		mv ".git" ".git_darwin_esp8266"
	elif [ ! -d "$GIT_DARWIN_ESP32_DIR" ]; then
		mv ".git" ".git_darwin_esp32"
	elif [ ! -d "$GIT_ARDUINO_LIBS_DIR" ]; then
		mv ".git" ".git_arduino_libs"
	fi
fi

echo ""
echo ""
echo "***************************Mixly2.0 安装^&升级助手****************************"
echo "感谢您选择使用Mixly2.0软件，如果您是第一次使用本软件，请仔细阅读本说明。"
echo ""
echo ""
echo "目前Mixly2.0软件支持对多种硬件的编程，因为支持所有硬件编程需要占用您硬盘较大的空间，您可以选择只安装您需要用到的部分功能即可。"
echo ""
echo "Mixly2.0软件支持的硬件，具体说明如下："
echo "MicroPython/CircuitPython ESP32系列开发板(MicroPython[ESP32_MixGo]、CircuitPython[ESP32S2_MixGoCE]等)为默认支持，无需选择。"
echo "Python(纯Python,无需硬件，支持图形化和代码互相转换)为默认支持，无需选择。"
echo "Arduino AVR系列开发板（包括Arduino UNO,Nano,Mega 2560,Pro Mini 等为默认支持，无需选择。"
echo "Arduino ESP8266系列开发板（包括WeMos D1,NodeMCU 等）"
echo "Arduino ESP32系列开发板（包括MixGo,普通ESP32开发板等）"
echo ""
echo ""
echo "安装和升级即将开始，您可以选择安装部分功能，输入y表示安装该功能，输入n表示不安装该功能。"
echo ""
echo ""

read -p "安装 Arduino ESP8266 (y/n):" installESP8266
if [ "$installESP8266" = "n" ]; then
	installESP8266="n"
else
	installESP8266="y"
fi
echo "$installESP8266"

read -p "安装 Arduino ESP32 (y/n):" installESP32
if [ "$installESP32" = "n" ]; then
	installESP32="n"
else
	installESP32="y"
fi
echo "$installESP32"

git config --global --add safe.directory "$DIR"
git config --system core.longpaths true
echo ""
echo ""
echo "更新Mixly2.0"
if [ -d "$GIT_MIXLY_DIR" ]; then
	mv ".git_mixly" ".git"
	if [ -d "$GIT_DIR" ]; then
		git fetch --all
		git reset --hard
		git pull
		git submodule init
		git submodule update
		git submodule foreach "git fetch --all && git reset --hard origin/master && git pull origin master"
		git gc
		git prune
		git clean -f .git/index.lock
		echo "Mixly2.0更新完成"
		mv ".git" ".git_mixly"
	fi
else
	echo "Cloning Mixly2.0..."
	if [ ! -d "$GIT_CLONE_DIR" ]; then
		mkdir "$GIT_CLONE_DIR"
	fi
	git clone "https://gitee.com/mixly2/mixly2.0-darwin-x64.git" "$GIT_CLONE_DIR"
	cp -a "$GIT_CLONE_DIR/." "$DIR"
	git submodule init
	git submodule update
	git submodule foreach "git fetch --all && git reset --hard origin/master && git pull origin master"
	rm -rf "$GIT_CLONE_DIR"
	git clean -f .git/index.lock
	echo "Mixly2.0更新完成"
	mv ".git" ".git_mixly"
fi

echo ""
echo ""

if [ -d "$GIT_DARWIN_AVR_DIR" ]; then
	mv ".git_darwin_avr" ".git"
	if [ -d "$GIT_DIR" ]; then
		git fetch --all
		git reset --hard
		git pull
		git gc
		git prune
		git clean -f .git/index.lock
		echo "Arduino AVR更新完成"
		mv ".git" ".git_darwin_avr"
	fi
else
	echo "Cloning Arduino AVR..."
	if [ ! -d "$GIT_CLONE_DIR" ]; then
		mkdir "$GIT_CLONE_DIR"
	fi
	git clone "https://gitee.com/mixly2/darwin_x64_avr.git" "$GIT_CLONE_DIR"
	cp -a "$GIT_CLONE_DIR/." "$DIR"
	rm -rf "$GIT_CLONE_DIR"
	git clean -f .git/index.lock
	echo "Arduino AVR更新完成"
	mv ".git" ".git_darwin_avr"
fi

echo ""
echo ""

if [ -d "$GIT_ARDUINO_LIBS_DIR" ]; then
	mv ".git_arduino_libs" ".git"
	if [ -d "$GIT_DIR" ]; then
		git fetch --all
		git reset --hard
		git pull
		git gc
		git prune
		git clean -f .git/index.lock
		echo "Arduino Libs更新完成"
		mv ".git" ".git_arduino_libs"
	fi
else
	echo "Cloning Arduino Libs..."
	if [ ! -d "$GIT_CLONE_DIR" ]; then
		mkdir "$GIT_CLONE_DIR"
	fi
	git clone "https://gitee.com/mixly2/arduino-libs.git" "$GIT_CLONE_DIR"
	cp -a "$GIT_CLONE_DIR/." "$DIR"
	rm -rf "$GIT_CLONE_DIR"
	git clean -f .git/index.lock
	echo "Arduino Libs更新完成"
	mv ".git" ".git_arduino_libs"
fi

echo ""
echo ""

if [ "$installESP8266" = "y" ]; then
	if [ -d "$GIT_DARWIN_ESP8266_DIR" ]; then
		mv ".git_darwin_esp8266" ".git"
		if [ -d "$GIT_DIR" ]; then
			git fetch --all
			git reset --hard
			git pull
			git gc
			git prune
			git clean -f .git/index.lock
			echo "Arduino ESP8266更新完成"
			mv ".git" ".git_darwin_esp8266"
		fi
	else
		echo "Cloning Arduino ESP8266..."
		if [ ! -d "$GIT_CLONE_DIR" ]; then
			mkdir "$GIT_CLONE_DIR"
		fi
		git clone "https://gitee.com/mixly2/darwin_esp8266.git" "$GIT_CLONE_DIR"
		cp -a "$GIT_CLONE_DIR/." "$DIR"
		rm -rf "$GIT_CLONE_DIR"
		git clean -f .git/index.lock
		echo "Arduino ESP8266更新完成"
		mv ".git" ".git_darwin_esp8266"
	fi
	echo ""
	echo ""
else
	if [ -f "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp8266/index_board_Arduino_ESP8266.html" ]; then
		rm "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp8266/index_board_Arduino_ESP8266.html"
	fi

	if [ -d "$GIT_DARWIN_ESP8266_DIR" ]; then
		rm -rf "$GIT_DARWIN_ESP8266_DIR"
	fi

	if [ -d "$DIR/arduino-cli/Arduino15/packages/esp8266" ]; then
		rm -rf "$DIR/arduino-cli/Arduino15/packages/esp8266"
	fi

	if [ -f "$DIR/arduino-cli/Arduino15/package_esp8266com_index.json" ]; then
		rm -rf "$DIR/arduino-cli/Arduino15/package_esp8266com_index.json"
	fi
fi

if [ "$installESP32" = "y" ]; then
	if [ -d "$GIT_DARWIN_ESP32_DIR" ]; then
		mv ".git_darwin_esp32" ".git"
		if [ -d "$GIT_DIR" ]; then
			git fetch --all
			git reset --hard
			git pull
			git gc
			git prune
			git clean -f .git/index.lock
			echo "Arduino ESP32更新完成"
			mv ".git" ".git_darwin_esp32"
		fi
	else
		echo "Cloning Arduino ESP32..."
		if [ ! -d "$GIT_CLONE_DIR" ]; then
			mkdir "$GIT_CLONE_DIR"
		fi
		git clone "https://gitee.com/mixly2/darwin_x64_esp32.git" "$GIT_CLONE_DIR"
		cp -a "$GIT_CLONE_DIR/." "$DIR"
		rm -rf "$GIT_CLONE_DIR"
		git clean -f .git/index.lock
		echo "Arduino ESP32更新完成"
		mv ".git" ".git_darwin_esp32"
	fi
	echo ""
	echo ""
else
	if [ -f "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32/index_board_Arduino_ESP32.html" ]; then
		rm "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32/index_board_Arduino_ESP32.html"
	fi

	if [ -f "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32c3/index.html" ]; then
		rm "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32c3/index.html"
	fi

	if [ -f "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32s2/index.html" ]; then
		rm "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32s2/index.html"
	fi

	if [ -f "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32s3/index.html" ]; then
		rm "$DIR/Mixly.app/Contents/Resources/app/board/arduino_esp32s3/index.html"
	fi

	if [ -d "$GIT_DARWIN_ESP32_DIR" ]; then
		rm -rf "$GIT_DARWIN_ESP32_DIR"
	fi

	if [ -d "$DIR/arduino-cli/Arduino15/packages/esp32" ]; then
		rm -rf "$DIR/arduino-cli/Arduino15/packages/esp32"
	fi

	if [ -f "$DIR/arduino-cli/Arduino15/package_esp32_index.json" ]; then
		rm -rf "$DIR/arduino-cli/Arduino15/package_esp32_index.json"
	fi
fi

chmod -R 777 "$DIR"
xattr -r -d com.apple.quarantine "$DIR"
echo "Mixly2.0 和 Arduino 包更新完成，Enjoy it!"